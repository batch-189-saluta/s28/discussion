// CRUD OPERATION

// INSERT DOCUMENTS (CREATE)
/*
	
	Syntax:
		Insert One Document
			db.collectionName.insertOne ({
				"fieldA": "valueA",
				"fieldB": "valueB",
			})
			*/


			db.users.insertOne({
				"firstName": "Jane",
				"lastName": "Doe",
				"age": 21,
				"email": "janedoe@mail.com",
				"department": "none"
			})

// ----------------------------------------------
// INSERT MANY DOCUMENTS
/*
	gumagamit ng ([])
	Syntax:
		db.collectionName.insertMany([
			{
				"fieldA": "valueA",
				"fieldB": "valueB"
			},
			{
				"fieldA": "valueA",
				"fieldB": "valueB"
			}

		])
		*/

db.users.insertMany([
		{
			"firstName": "Stephen",
			"lastName": "Hawking",
			"age": 76,
			"email": "stephenhawking@mail.com",
			"department": "none"
		},
		{
			"firstName": "Neil",
			"lastName": "Armstrong",
			"age": 82,
			"email": "neilarmstron@mail.com",
			"department": "none"
		}


])

// -----------------------------------
db.courses.insertMany([
		{
			"name": "Javascript 101",
			"price": 5000,
			"description": "Introduction to Javascript",
			"isActive": true
		},
		{
			"name": "HTML 101",
			"price": 2000,
			"description": "Introduction to HTML",
			"isActive": true
		},
		{
			"name": "CSS 101",
			"price": 2500,
			"description": "Introduction to CSS",
			"isActive": false
		}



	])

// -----------------------------------------------------
// RETRIEVING /FIND DOCUMENTS (READ)
/*
	Syntax:

		db.collectionName.find() 
		- this will retrieve all our documents (pag walang nakaset na criteria)

		db.collectionName.find({"criteria": "value"}) 
		- this will retrieve all the documents that will match our criteria

		db.collectionName.findOne({"criteria": value})
		-this will return the first document in our collection that match the criteria

		db.collectionName.findOOne({})
		-will return the first document in our collection

*/
db.users.find();

db.users.findOne({
	"firstName": "Jane"
})

db.users.find({
	"lastName": "Armstrong",
	"age": 82
})


// ----------------------------------------------------
// UPDATING DOCUMENTS (Update)
/*
	Syntax:
		db.collectionName.updateOne({
			"criteria": "value"

		},
		{
			$set: {
				"fieldToBeUpdated": "updatedValue"
			}
		})
		


		db.collectionName.updateMany(
		{
			"criteria": "value"
		},
		{
			$set: {
				"fieldToBeUpdated": "updatedValue"
			}
		}
		)
*/

db.users.insertOne({
	"firstName": "Test",
	"lastName": "Test",
	"age": 0,
	"email": "test@gmail.com",
	"department": "none"
})

// updating one document--------------------------
db.users.updateOne(
		{
			"firstName": "Test"
		},
		{
			$set: {
				"firstName": "Bill",
				"lastName": "Gates",
				"age": 65,
				"email": "billgates@mail.com",
				"department": "Operations",
				"status": "active"
			}
		}

	)

// REMOVING A FIELD----------------------------------
db.users.updateOne(
		{
			"firstName": "Bill"
		},
		{
			$unset: {
				"status": "active"
			}
		}

	)

// Updating Multiple Documents--------------------------
db.users.updateMany(
		{
			"department": "none"
		},
		{
			$set: {
				"department": "HR"
			}
		}

	)


// Renaming a field------------------------------------------------
db.users.updateMany(
		{},
		{
			$rename: {
				"department": "dept"
			}
		}

	)



db.courses.updateOne(
		{
			"name": "Javascript 101"
		},
		{
			$set: {
				"isActive": true
			}
		}

	)

db.courses.updateOne(
		{
			"name": "HTML 101"
		},
		{
			$set: {
				"isActive": false
			}
		}

	)

db.courses.updateMany(
		{},
		{
			$set: {
				"enrollees": 10
			}
		}
	)



// ----------------------------------------------------
// DELETING DOCUMENTS(DELETE)

db.users.insertOne({
	"firstName": "Test"
})

/*
	Deleting a single document:

		Syntax:
			db.collectionName.deleteOne({"criteria": "value"})

*/

db.users.deleteOne({
	"firstName": "Test"
})

// Deleeting multiple documents-------------------------------------------
/*
	Syntax:
		dbcollectionName.deleteMany({"criteria":"value"}) 
*/

db.users.deleteMany({
	"dept": "HR"
})

db.courses.deleteMany({})